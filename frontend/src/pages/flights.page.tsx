import { FlightCard } from "../components";
import { useFlights } from "../hooks";

export function FlightsPage() {
  const { data: flights } = useFlights();

  return (
    <section>
      <h1>Scheduled Flights</h1>

      <div style={{ display:'flex', flexDirection:'column', gap: "0.5rem" }}>
        {flights?.map((flight) => (
          <FlightCard
            key={flight._id}
            origin={flight.origin}
            destination={flight.destination}
            code={flight.code}
            status={flight.status}
          />
        ))}
      </div>
    </section>
  );
}
